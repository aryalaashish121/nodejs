const express = require('express');
const morgan = require('morgan');
require('./database/index');
require('dotenv').config();
const userRoute = require('./routes/users');
const blogRoute = require('./routes/blogs');
const authRoute = require('./routes/auth');
const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan('dev'));
class Server {
    constructor() {
        this.routes();
        this.startServer();

    }
    routes() {
        const route = process.env.DEFAULT_ROUTE;
        app.use(`${route}`, authRoute);
        app.use(`${route}/user`, userRoute);
        app.use(`${route}/blog`, blogRoute);
    }

    startServer() {
        app.listen(process.env.APP_PORT || 3000, () => {
            console.log(`Application listning in port ${process.env.APP_PORT}`);
        });
    }
}
new Server();
