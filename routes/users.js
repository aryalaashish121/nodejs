const express = require('express');
const router = express.Router();
const { index, store, edit, update, destroy } = require('../controllers/UserController');

//login&& register

router.get('/', index)
router.post('/', store);
router.get('/:id/edit', edit);
router.put('/:id', update);
router.delete('/:id', destroy);

module.exports = router;

