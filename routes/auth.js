const express = require('express');
const router = express.Router();
const { register } = require('../controllers/auth/RegisterController');
const { login } = require('../controllers/auth/LoginController');

router.post('/register', register);
router.get('/login', login);

module.exports = router;