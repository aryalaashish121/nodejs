const express = require('express');
const { index, store, edit, update, destroy } = require('../controllers/BlogController');
const router = express.Router();
const multer = require('multer');
const auth = require('../middlewares/auth');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/images')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.jpg') //Appending .jpg
    }
})
var upload = multer({ storage: storage });


router.get('/', index);
router.post('/', auth, upload.single('image'), store);
router.get('/:id/edit', auth, edit);
router.put('/:id', auth, update);
router.delete('/:id', auth, destroy);
module.exports = router;