
const User = require('../models/User');
exports.index = (req, res, next) => {
    User.find((err, data) => {
        if (err) {
            throw err;
        }
        res.status(200).send(data);
    });
}

exports.store = (req, res, next) => {
    try {
        const user = new User(req.body);
        user.save((err, data) => {
            console.log(err);
            if (err) {
                return res.status(400).send({ error: err });
            }
            res.status(201).send({ data: data });
        })
    } catch (error) {
        res.status(400).send({ error: error });
        console.log(error);
    }
}

exports.show = async (req, res, next) => {
    await User.findById(req.params.id, (err, data) => {
        if (err) {
            return res.status(404).send({ "error": "Data not avaiable!" });
        }
        res.status(200).send({
            "data": data,
        });
    });

}

exports.edit = async (req, res, next) => {
    await User.findById(req.params.id, (err, data) => {
        if (err) {
            return res.status(404).send({ "error": "Not found" });
        }
        res.status(200).send({
            "data": data,
        });
    });

}

exports.destroy = async (req, res, next) => {
    try {
        console.log(req);
        var _user = await User.findById(req.params.id);
        if (!_user) return res.status(404).send({ "error": "User not found!", "success": false });
        User.findOneAndDelete(req.params.id, (err, data) => {
            if (err) {
                res.status(404).send(err);
            }
            res.status(200).send({ "message": "Deleted successfully" });
        })
    } catch (error) {
        console.log(error);
    }

}

exports.update = (req, res, next) => {
    res.send("Update comming soon!");
}
