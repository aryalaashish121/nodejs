const Blog = require('../models/Blog');

exports.index = (req, res, next) => {
    Blog.find({}, (err, data) => {
        if (err) {
            res.send(err);
        }
        res.status(200).send({ "data": data });
    })
}

exports.store = async (req, res, next) => {
    console.log("saving to database");
    let data = new Blog(req.body);
    data.image = req.file.filename;
    data.userId = req.auth._id;
    try {
        await data.save((err, data) => {
            if (err) {
                throw err;
            }
            res.status(201).json({ "data": data });
        })
    } catch (error) {
        res.send(error);
    }

}

exports.edit = async (req, res, next) => {
    console.log("get blog by id..");
    try {
        Blog.findById(req.params.id, (err, blog) => {
            if (err) return res.status(500).send({ "error": "Server error! Could not find data!" });
            if (!blog) return res.status(404).send({ "error": "Blog not found!" });
            res.status(200).send({ "data": blog, "success": true });
        })
    } catch (error) {
        res.status(500).send(err);
        console.log(error);
    }
}

exports.update = async (req, res, next) => {
    try {
        var _blog = await Blog.findById(req.params.id);
        if (!(_blog.userId.equals(req.auth._id))) {
            return res.status(401).send({ "error": "You are unauthorized!", "success": false });
        }
        //check if exist
        var blog = await Blog.findOneAndUpdate({ _id: req.params.id }, { $set: req.body });
        if (!blog) return res.status(500).send({ "error": "Could not update blog!", "success": false });
        var updatedBlog = await Blog.findById(blog._id);
        res.status(200).send({ "success": true, "data": updatedBlog });
    } catch (error) {
        res.send(error);
        console.log(error);
    }
}

exports.destroy = async (req, res, next) => {
    try {
        var _blog = await Blog.findById(req.params.id);
        if (!_blog) return res.status(404).send({ "error": "Blog not found!", "success": false })
        if (!(_blog.userId.equals(req.auth._id))) {
            return res.status(401).send({ "error": "You are unauthorized!", "success": false });
        }
        //check if exist
        var blog = await Blog.findByIdAndDelete(req.params.id);
        res.status(200).send({ "success": true, "message": "Deleted successfully!" });
    } catch (error) {
        res.send(error);
        console.log(error);
    }
}
