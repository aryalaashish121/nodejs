const User = require("../../models/User");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { validateUser } = require("../../validation/UserValidation");

exports.login = (req, res, next) => {
    try {
        //validate user
        const { username, password } = req.body;
        if (!validateUser(username, password)) res.status(422).send({ "error": "Username and password required!" });

        User.findOne({ username: username }, (err, user) => {
            if (err) return res.status(400).json({ "err": err });
            if (!user) return res.status(404).json({
                "error": "Could not find provided credentials. Please register!"
            });

            let checkPassword = bcrypt.compareSync(password, user.password);
            if (!checkPassword) return res.status(401).send({ "error": "Invalid credentials!" });
            var token = jwt.sign({ userId: user._id, username }, process.env.JWT_TOKEN_KEY, { expiresIn: process.env.TOKEN_EXPIREIN });
            res.status(200).send({
                "message": "Logged in successfully",
                "token": token
            });
        })
    } catch (error) {
        res.send(error);
    }
}