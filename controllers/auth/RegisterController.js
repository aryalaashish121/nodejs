const res = require("express/lib/response");
const User = require("../../models/User");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { validateUser, checkDuplicateUser } = require('../../validation/UserValidation');

exports.register = async (req, res, next) => {
    const { username, password } = req.body;

    try {
        if (!validateUser(username, password)) return res.status(422).send({ "error": "Username and password required" });
        if (checkDuplicateUser(username)) return res.status(422).send({ "error": "User already exsit" });
        //check if user exist or not
        let encriptedPassword = await bcrypt.hash(password, 10);

        //create user 
        const user = await User.create({
            username: username.toLowerCase(),
            password: encriptedPassword
        });

        const token = jwt.sign({
            userId: user._id
        },
            process.env.JWT_TOKEN_KEY, {
            expiresIn: process.env.TOKEN_EXPIREIN,
        });

        // save user token
        user.token = token;

        res.status(201).json({
            data: user,
            success: true,
            message: "Register successfully"
        });
    } catch (err) {
        console.log(err);
        res.status(408).send(err);
    }

};
