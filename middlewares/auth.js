const jwt = require('jsonwebtoken');
const User = require('../models/User');

const auth = (req, res, next) => {

    var token = req.headers['x-access-token'];
    console.log(token);
    if (!token) return res.status(401).send({ "error": "You are unauthenticated! Please login." });

    jwt.verify(token, process.env.JWT_TOKEN_KEY, (err, user) => {
        if (err) return res.status(500).send({ "error": "Failed to validate access!", "error1": err });
        console.log(user);
        User.findById(user.userId, (err, userData) => {
            if (err) return res.status(500).send({ "error": "Failed to access user!" });
            if (!userData) return res.status(401).send({ "error": "User not found!" });
            req.auth = {
                username: userData.username,
                _id: userData._id,
            };
            next();
        })
    })
}

module.exports = auth;