const mongoose = require('mongoose');
const slugify = require('slugify');
const blogSchema = mongoose.Schema({
    userId: {
        type: mongoose.Schema.ObjectId,
        ref: "User",
        required: true,
    },
    title: {
        type: String
    },
    slug: {
        type: String,
        unique: true,
    },
    description: {
        type: String,
    },
    image: {
        type: String,
    },
    published_date: {
        type: Date,
    },

}, {
    timestamps: true
});

blogSchema.pre('save', function (next) {
    this.title = this.title.toUpperCase();
    next();
});

blogSchema.pre('save', function (next) {
    this.slug = slugify(this.title, { lower: true });
    next();
});

module.exports = mongoose.model('Blog', blogSchema);