const mongoose = require('mongoose');
const userSchema = mongoose.Schema({
    username: {
        required: true,
        type: String,
        unique: true,
    },
    password: {
        required: true,
        type: String,
    },
    token: {
        type: String,
    }
});
module.exports = mongoose.model('User', userSchema);